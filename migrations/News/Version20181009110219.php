<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Broadway\EventStore\Dbal\DBALEventStore;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManager;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181009110219 extends AbstractMigration implements ContainerAwareInterface
{
    /** @var EntityManager */
    private $em;

    /** @var DBALEventStore */
    private $eventStore;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->em = $container->get('doctrine.orm.entity_manager');
        $this->eventStore = $container->get(DBALEventStore::class);
    }

    public function up(Schema $schema) : void
    {
        $this->eventStore->configureSchema($schema);
        $this->em->flush();
    }

    public function down(Schema $schema) : void
    {
        $schema->dropTable('api.events');
        $this->em->flush();
    }
}
