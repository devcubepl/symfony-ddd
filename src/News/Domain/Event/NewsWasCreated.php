<?php
declare(strict_types=1);

namespace App\News\Domain\Event;

use App\News\Domain\ValueObject\NewsContent;
use App\News\Domain\ValueObject\NewsStatus;
use App\News\Domain\ValueObject\NewsTitle;
use Broadway\Serializer\Serializable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Assert\Assertion;

class NewsWasCreated implements Serializable
{
    /**
     * @var UuidInterface
     */
    public $uuid;

    /**
     * @var NewsTitle
     */
    public $title;

    /**
     * @var NewsContent
     */
    public $content;

    /**
     * @var NewsStatus
     */
    public $status;

    /**
     * NewsWasCreated constructor.
     * @param UuidInterface $uuid
     * @param NewsTitle $title
     * @param NewsContent $content
     * @param NewsStatus $status
     */
    public function __construct(UuidInterface $uuid, NewsTitle $title, NewsContent $content, NewsStatus $status)
    {
        $this->uuid = $uuid;
        $this->title = $title;
        $this->content = $content;
        $this->status = $status;
    }

    /**
     * @throws \Assert\AssertionFailedException
     */
    public static function deserialize(array $data): self
    {
        Assertion::keyExists($data, 'uuid');
        Assertion::keyExists($data, 'title');
        Assertion::keyExists($data, 'content');
        Assertion::keyExists($data, 'status');

        return new self(
            Uuid::fromString($data['uuid']),
            NewsTitle::fromString($data['title']),
            NewsContent::fromString($data['title']),
            NewsStatus::create((int)$data['status'])
        );
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        return [
            'uuid' => $this->uuid->toString(),
            'title' => $this->title->toString(),
            'content' => $this->content->toString(),
            'status' => $this->status->toString()
        ];
    }
}
