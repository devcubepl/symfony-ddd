<?php
declare(strict_types=1);

namespace App\News\Domain\ValueObject;


use Assert\Assertion;

class NewsTitle
{
    private $title;

    public static function fromString(string $raw)
    {
        Assertion::minLength($raw, 2, 'Min 2 characters length');
        Assertion::maxLength($raw, 100, 'Too long title');

        $t = new self();
        $t->title = $raw;

        return $t;
    }

    public function toString(): string
    {
        return $this->title;
    }

    public function __toString(): string
    {
        return $this->title;
    }

    private function __construct()
    {
    }
}
