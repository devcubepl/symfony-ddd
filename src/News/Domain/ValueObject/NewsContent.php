<?php
declare(strict_types=1);

namespace App\News\Domain\ValueObject;

use Assert\Assertion;

class NewsContent
{
    /**
     * @throws \Assert\AssertionFailedException
     */
    public static function fromString(string $raw): self
    {
        Assertion::minLength($raw, 3, 'Min 3 characters content');
        Assertion::maxLength($raw, 5000, 'Too long content');

        $t = new self();
        $t->content = $raw;

        return $t;
    }

    public function toString(): string
    {
        return $this->content;
    }

    public function __toString(): string
    {
        return $this->content;
    }

    private function __construct()
    {
        // nothing here
    }

    private $content;
}
