<?php
declare(strict_types=1);

namespace App\News\Domain\ValueObject;

use Assert\Assertion;

class NewsStatus
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    const STATUS_DRAFT = 3;

    /**
     * @var integer
     */
    private $status;

    /**
     * @param mixed $status
     * @return NewsStatus
     */
    public static function fromString($status)
    {
        return self::create((int)$status);
    }

    /**
     * @param int $status
     * @return NewsStatus
     */
    public static function create(int $status)
    {
        Assertion::inArray($status, [
            self::STATUS_INACTIVE,
            self::STATUS_DRAFT,
            self::STATUS_ACTIVE
        ], 'Given status value is not valid!');

        $s = new self();
        $s->status = $status;

        return $s;
    }

    /**
     * @param int $id
     * @return array|string
     */
    public static function getLabel($id = null)
    {
        $data = [
            self::STATUS_INACTIVE => 'Inactive',
            self::STATUS_DRAFT => 'Draft',
            self::STATUS_ACTIVE => 'Active',
        ];

        if (!is_null($id)) {
            return $data[$id] ?? $id;
        }

        return $data;
    }

    /**
     * @param boolean $label
     * @return string
     */
    public function toString($label = false): string
    {
        return ($label)? self::getLabel($this->status) : (string)$this->status;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->status;
    }

    private function __construct()
    {

    }
}
