<?php
declare(strict_types=1);

namespace App\News\Domain\Repository;

use App\News\Domain\News;
use Ramsey\Uuid\UuidInterface;

interface NewsRepositoryInterface
{
    public function get(UuidInterface $uuid): News;

    public function store(News $news): void;
}
