<?php
declare(strict_types=1);

namespace App\News\Domain;

use App\News\Domain\Event\NewsWasCreated;
use Ramsey\Uuid\UuidInterface;
use App\News\Domain\ValueObject\NewsContent;
use App\News\Domain\ValueObject\NewsStatus;
use App\News\Domain\ValueObject\NewsTitle;
use Broadway\EventSourcing\EventSourcedAggregateRoot;

class News extends EventSourcedAggregateRoot
{
    public static function create(UuidInterface $uuid, NewsTitle $title, NewsContent $content, NewsStatus $status)
    {
        $news = new self();
        $news->apply(new NewsWasCreated($uuid, $title, $content, $status));

        return $news;
    }

    protected function applyNewsWasCreated(NewsWasCreated $event): void
    {
        $this->uuid = $event->uuid;
        $this->title = $event->title;
        $this->content = $event->content;
        $this->status = $event->status;
    }

    /**
     * @return string
     */
    public function getAggregateRootId(): string
    {
        return $this->uuid->toString();
    }

    /** @var UuidInterface */
    private $uuid;

    /**
     * @var NewsTitle
     */
    private $title;

    /**
     * @var NewsContent
     */
    private $content;

    /**
     * @var NewsStatus
     */
    private $status;
}
