<?php
declare(strict_types=1);

namespace App\News\Infrastructure\Repository;


use App\News\Domain\News;
use App\News\Domain\Repository\NewsRepositoryInterface;
use Broadway\EventHandling\EventBus;
use Broadway\EventSourcing\AggregateFactory\AggregateFactory;
use Broadway\EventSourcing\AggregateFactory\PublicConstructorAggregateFactory;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\EventStore\EventStore;
use Ramsey\Uuid\UuidInterface;

class NewsStore extends EventSourcingRepository implements NewsRepositoryInterface
{
    public function __construct(EventStore $eventStore, EventBus $eventBus, $eventStreamDecorators = [])
    {
        $aggregateFactory = new PublicConstructorAggregateFactory();
        parent::__construct($eventStore, $eventBus, News::class, $aggregateFactory, $eventStreamDecorators);
    }

    public function get(UuidInterface $uuid): News
    {
        return $this->load((string)$uuid);
    }

    public function store(News $news): void
    {
        $this->save($news);
    }
}
