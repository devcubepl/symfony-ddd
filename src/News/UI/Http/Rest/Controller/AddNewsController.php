<?php
declare(strict_types=1);

namespace App\News\UI\Http\Rest\Controller;


use App\Common\UI\Http\Rest\Controller\CommandController;
use App\News\Application\Command\AddNewsCommand;
use App\News\Domain\ValueObject\NewsStatus;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

class AddNewsController extends CommandController
{
    /**
     * @Route(
     *     "/",
     *     name="news_create",
     *     methods={"POST"}
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="News created successfully"
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Bad request"
     * )
     * @SWG\Response(
     *     response=409,
     *     description="Conflict"
     * )
     *
     * @SWG\Parameter(
     *     name="news",
     *     type="object",
     *     in="body",
     *     required=true,
     *     schema=@SWG\Schema(type="object",
     *         @SWG\Property(property="title", type="string"),
     *         @SWG\Property(property="content", type="string"),
     *         @SWG\Property(property="status", type="integer"),
     *     )
     * )
     *
     * @SWG\Tag(name="News")
     *
     * @throws \Assert\AssertionFailedException
     * @throws \Exception
     */
    public function __invoke()
    {
        $command = new AddNewsCommand(
            Uuid::uuid1()->toString(),
            'Clickbait!',
            'Bardzo wazna wiadomosc',
            1
        );
        $this->exec($command);
    }
}
