<?php
declare(strict_types=1);

namespace App\News\UI\Http\Web\Controller;

use App\Common\UI\Http\Web\Controller\AbstractWebController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractWebController
{
    /**
     * @Route(
     *     "/",
     *     name="homepage",
     *     methods={"GET"}
     * )
     * @return Response
     */
    public function index()
    {
        return $this->render('News/Web/home/index.html.twig');
    }
}
