<?php
declare(strict_types=1);

namespace App\News\Application\Command;


use App\News\Domain\ValueObject\NewsContent;
use App\News\Domain\ValueObject\NewsStatus;
use App\News\Domain\ValueObject\NewsTitle;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class AddNewsCommand
{
    /**
     * @var UuidInterface
     */
    private $uuid;

    /**
     * @var NewsTitle
     */
    private $title;

    /**
     * @var NewsContent
     */
    private $content;

    /**
     * @var NewsStatus
     */
    private $status;

    /**
     * AddNewsCommand constructor.
     * @param string $uuid
     * @param $title
     * @param $content
     * @param $status
     * @throws \Assert\AssertionFailedException
     */
    public function __construct(string $uuid, $title, $content, $status)
    {
        $this->uuid = Uuid::fromString($uuid);
        $this->title = NewsTitle::fromString($title);
        $this->content = NewsContent::fromString($content);
        $this->status  = NewsStatus::create($status);
    }

    /**
     * @return UuidInterface
     */
    public function uuid(): UuidInterface
    {
        return $this->uuid;
    }

    /**
     * @return NewsTitle
     */
    public function title(): NewsTitle
    {
        return $this->title;
    }

    /**
     * @return NewsContent
     */
    public function content(): NewsContent
    {
        return $this->content;
    }

    /**
     * @return NewsStatus
     */
    public function status(): NewsStatus
    {
        return $this->status;
    }
}
