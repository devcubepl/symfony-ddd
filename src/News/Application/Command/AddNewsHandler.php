<?php
declare(strict_types=1);

namespace App\News\Application\Command;


use App\Common\Application\Command\CommandHandlerInterface;
use App\News\Domain\News;
use App\News\Domain\Repository\NewsRepositoryInterface;

class AddNewsHandler implements CommandHandlerInterface
{
    private $newsRepository;

    /**
     * AddNewsHandler constructor.
     * @param NewsRepositoryInterface $newsRepository
     */
    public function __construct(NewsRepositoryInterface $newsRepository)
    {
        $this->newsRepository = $newsRepository;
    }

    public function handle(AddNewsCommand $command): void
    {
        $newsRoot = News::create(
            $command->uuid(),
            $command->title(),
            $command->content(),
            $command->status()
        );

        $this->newsRepository->store($newsRoot);
    }
}
