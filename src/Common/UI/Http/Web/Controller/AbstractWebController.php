<?php
declare(strict_types=1);

namespace App\Common\UI\Http\Web\Controller;

use Symfony\Component\HttpFoundation\Response;

class AbstractWebController
{
    private $template;

    public function __construct(\Twig_Environment $template)
    {
        $this->template = $template;
    }

    /**
     * @param string $view
     * @param array $parameters
     * @param int $code
     * @return Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    protected function render(string $view, array $parameters = [], int $code = Response::HTTP_OK): Response
    {
        $content = $this->template->render($view, $parameters);
        return new Response($content, $code);
    }
}
