<?php
declare(strict_types=1);

namespace App\Common\UI\Http\Rest\Controller;

use League\Tactician\CommandBus;

abstract class CommandController
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * CommandController constructor.
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    protected function exec($command): void
    {
        $this->commandBus->handle($command);
    }
}
